<?php
/**
 * @file
 * aws_glacier_fields.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function aws_glacier_fields_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'glacier_job-glacier_job-field_archive_ref'.
  $field_instances['glacier_job-glacier_job-field_archive_ref'] = array(
    'bundle' => 'glacier_job',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'glacier_job',
    'field_name' => 'field_archive_ref',
    'label' => 'Archive',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'glacier_job-glacier_job-field_completiondate'.
  $field_instances['glacier_job-glacier_job-field_completiondate'] = array(
    'bundle' => 'glacier_job',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'glacier_job',
    'field_name' => 'field_completiondate',
    'label' => 'CompletionDate',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'glacier_job-glacier_job-field_creationdate'.
  $field_instances['glacier_job-glacier_job-field_creationdate'] = array(
    'bundle' => 'glacier_job',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'glacier_job',
    'field_name' => 'field_creationdate',
    'label' => 'CreationDate',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'glacier_job-glacier_job-field_joboutput_ref'.
  $field_instances['glacier_job-glacier_job-field_joboutput_ref'] = array(
    'bundle' => 'glacier_job',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'glacier_job',
    'field_name' => 'field_joboutput_ref',
    'label' => 'JobOutput',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'glacier_job-glacier_job-field_statuscode'.
  $field_instances['glacier_job-glacier_job-field_statuscode'] = array(
    'bundle' => 'glacier_job',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'glacier_job',
    'field_name' => 'field_statuscode',
    'label' => 'StatusCode',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'glacier_job-glacier_job-field_vault_ref'.
  $field_instances['glacier_job-glacier_job-field_vault_ref'] = array(
    'bundle' => 'glacier_job',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'glacier_job',
    'field_name' => 'field_vault_ref',
    'label' => 'Vault',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Archive');
  t('CompletionDate');
  t('CreationDate');
  t('JobOutput');
  t('StatusCode');
  t('Vault');

  return $field_instances;
}
