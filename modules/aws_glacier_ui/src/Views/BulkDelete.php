<?php

namespace Drupal\aws_glacier_ui\Views;

use Drupal\aws_glacier\Entity\Vault\Vault;
use Drupal\aws_glacier\Entity\Job\Job;

/**
 * Class BulkDelete.
 *
 * @package Drupal\aws_glacier_ui\Views
 */
class BulkDelete extends \views_handler_field_entity {

  /**
   * {@inheritDoc}
   */
  function render($values) {
    if ($entity = $this->get_value($values)) {
      return $this->render_link($entity);
    }
    return '';
  }

  /**
   * Returns the link text.
   *
   * Modified the field options on the fly to render the field as a link.
   *
   * @param \Drupal\aws_glacier\Entity\Vault\Vault $entity
   *  The vault entity.
   *
   * @return string
   *   Text of the link.
   */
  function render_link(Vault $entity) {
    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['query'] = drupal_get_destination();
    $job = Job::loadForVault($entity);
    if (($id = $job->identifier()) && ($completed = $job->getCompletionDate()) && !empty($completed) && $entity->NumberOfArchives > 0) {
      $this->options['alter']['path'] = AWS_GLACIER_ADMIN_PATH . '/jobs/' . $id . '/bulkdelete';
    }
    return t('Delete all archives');
  }

}
