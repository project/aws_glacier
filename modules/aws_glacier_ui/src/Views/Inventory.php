<?php

namespace Drupal\aws_glacier_ui\Views;

use Drupal\aws_glacier\Entity\Job\Job;
use Drupal\aws_glacier\Entity\Vault\Vault;

/**
 * Class Inventory.
 *
 * @package Drupal\aws_glacier_ui\Views
 */
class Inventory extends \views_handler_field_entity {

  /**
   * {@inheritDoc}
   */
  function render($values) {
    if ($entity = $this->get_value($values)) {
      return $this->render_link($entity);
    }
    return '';
  }

  /**
   * Returns the link text.
   *
   * Modified the field options on the fly to render the field as a link.
   *
   * @param \Drupal\aws_glacier\Entity\Vault\Vault $entity
   *  The vault entity.
   *
   * @return string
   *   Text of the link.
   */
  function render_link(Vault $entity) {
    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['query'] = drupal_get_destination();
    $job = Job::loadForVault($entity);
    if (($id = $job->identifier())) {
      $this->options['alter']['path'] = AWS_GLACIER_ADMIN_PATH . '/jobs/' . $id . '/status';
      return t('Inventory status: @status', array('@status' => $job->getStatusCode()));
    }
    else {
      $this->options['alter']['path'] = AWS_GLACIER_ADMIN_PATH . '/jobs/inventory/' . $entity->identifier() . '/create';
      return t('Initiate inventory');
    }
  }

}
