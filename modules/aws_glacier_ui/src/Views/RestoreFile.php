<?php

namespace Drupal\aws_glacier_ui\Views;

use Drupal\aws_glacier\Entity\Archive\Archive;
use Drupal\aws_glacier\Entity\Job\Job;

/**
 * Class RestoreFile.
 *
 * @package Drupal\aws_glacier_ui\Views
 */
class RestoreFile extends \views_handler_field_entity {

  /**
   * {@inheritDoc}
   */
  public function render($values) {
    if ($entity = $this->get_value($values)) {
      return $this->render_link($entity, $values);
    }
    return '';
  }

  /**
   * Returns the link text.
   *
   * Modified the field options on the fly to render the field as a link.
   *
   * @param \Drupal\aws_glacier\Entity\Archive\Archive $archive
   *  The archive entity.
   * @param $values array
   *  Values from database.
   *
   * @return string
   *   Text of the link.
   */
  public function render_link(Archive $archive, $values) {
    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['query'] = drupal_get_destination();
    /** @var \Drupal\aws_glacier\Entity\Vault\Vault $vault */
    $vault = entity_create('glacier_vault', array('VaultName' => $archive->vaultName));
    $vault = $vault->loadByUniqueProperty();
    $job = Job::loadForArchive($vault, $archive);
    if (($id = $job->identifier())) {
      $this->options['alter']['path'] = AWS_GLACIER_ADMIN_PATH . '/jobs/' . $id . '/status';
      return t('Restore status: @status', array('@status' => $job->getStatusCode()));
    }
    else {
      $this->options['alter']['path'] = AWS_GLACIER_ADMIN_PATH . '/jobs/archive/' . $this->get_value($values, 'id') . '/create';
      return t('Restore');
    }
  }

}
