<?php

namespace Drupal\aws_glacier\Exception;

/**
 * Class ExistsException.
 *
 * @package Drupal\aws_glacier\Exception
 */
class ExistsException extends \Exception {

}
