<?php

namespace Drupal\aws_glacier\Exception;

/**
 * Class VaultException.
 *
 * @package Drupal\aws_glacier\Exception
 */
class VaultException extends \Exception {

}
